# tam-rs

An implementation of the [Triangle Abstract Machine (TAM)](http://www.dcs.gla.ac.uk/~daw/books/PLPJ/) in Java.

The TAM is a Stack Machine with 16-bit data words and 32-bit instructions. The Stack Machine provides 16 special-purpose registers which are used to manipulate data and code addresses. The full specification of the TAM is found in [docs/tam_specification.md](docs/tam_specification.md).

## LICENCE

See [LICENCE](LICENSE.md).